const program = require('commander');
const path = require('path');
const fs = require('fs');
const websocket = require( './lib/websocket.js' );

program
  .option('-c, --config [configFile]', 'Specify the config file containing server options' )
  .parse(process.argv);

if ( program.config === undefined ) {
    console.log( 'No config file specified. Will use default options' );
    program.config = path.join( __dirname, 'default.config.json' );
}

if (!fs.existsSync(program.config)) {
    console.log( `ERROR! Cannot find config file ${program.config}` );
    process.exit(-1);
}

const contents = fs.readFileSync( program.config, 'UTF8' );
let CONFIG = {};
try {
    CONFIG = JSON.parse( contents )
} catch (e) {
    console.log( 'ERROR!', e );
    process.exit(-1);
}

websocket.create( CONFIG );