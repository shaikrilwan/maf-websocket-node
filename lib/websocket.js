const fs = require('fs');
const http = require('http');

class WebSocket {

    static create( options ) {

        return new Promise( (resolve, reject) => {

            // Loading the index file . html displayed to the client
            const server = http.createServer(function(req, res) {
                fs.readFile('./index.html', 'utf-8', function(error, content) {
                    res.writeHead(200, {"Content-Type": "text/html"});
                    res.end(content);
                });
            });

            // Loading socket.io
            var io = require('socket.io').listen(server);
            var connections = [];

            // We need to support socket.io namespace to prevent clash with outher socket.io
            // clients e.g. browsersync
            let socket = options.ns ? io.of( `/${options.ns}`) : io.sockets;

            // When a client connects, we note it in the console
            socket.on('connection', function (socket) {
            
                connections.push( socket ); 

                console.log( `[WebSocket] client connected [${connections.length}]` );

                socket.on( 'MESSAGE', (data) => {

                    // broadcast message
                    connections.forEach( client => {
                        if (data.toAll === true || client.id !== socket.id) {
                            client.emit( 'MESSAGE', data );
                        }
                    })

                });

                socket.on('disconnect', () => {

                    const ix = connections.findIndex( item => item.id === socket.id );
                    if (ix>=0) {
                        connections.splice( ix, 1 );
                    }

                    console.log( `[WebSocket] client disconnected [${connections.length}]` );

                });

            });

            server.listen(options.port, '0.0.0.0', null, () => {
                console.log( `[WebSocket] Running on port ${options.port}` );
                resolve( server );
            });
        });

    }
}


module.exports = WebSocket;