## Description
A simple little websocket relay service. Allows clients to send messages to each other!

## Setup

```
npm i
```

## To run

```
node server.js
```

This will use the default configuration options from the default.config.json file.

To specify a different config file use the following...

```
node service.js --config /path/to/json
```

The default json file looks like this...

```
{
    "port": 11099
}
```
